# MopliancesWarRebirth


This is a multiplayer game that could be played on PC and Mobile, made with Unity and Mirror.
The game was intended for mobile devices only, however, due to a bug that was discovered in the framework that was used for the multiplayer part of the game,
it fully functions on PC only. The team consisted of 2 members. 

- My team mate, which worked on the grapical side of the game
- And me, where I did the coding

Hristo Stoyanov 2020/2021
